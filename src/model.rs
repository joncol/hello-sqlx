use chrono::{NaiveDateTime, Utc};
use sqlx::{Database, Pool, Postgres};
use uuid::Uuid;

type QueryResult = Result<<Postgres as Database>::QueryResult, sqlx::Error>;

#[derive(Debug)]
pub(crate) struct Car {
    id: Uuid,
    brand: String,
    model: String,
    year: i32,
    created_at: NaiveDateTime,
}

pub(crate) async fn truncate_cars_table(pool: &Pool<Postgres>) -> QueryResult {
    sqlx::query!("truncate table cars").execute(pool).await
}

pub(crate) async fn create_car(pool: &Pool<Postgres>) -> QueryResult {
    sqlx::query!(
        "insert into cars (id, brand, model, year, created_at) \
         values ($1, $2, $3, $4, $5)",
        Uuid::new_v4(),
        "Volkswagen",
        "Golf",
        2018,
        Utc::now().naive_utc(),
    )
    .execute(pool)
    .await
}

pub(crate) async fn cars(pool: &Pool<Postgres>) -> Result<Vec<Car>, sqlx::Error> {
    sqlx::query_as!(Car, "select * from cars")
        .fetch_all(pool)
        .await
}
