use crate::config::Config;
use sqlx::{postgres::PgPoolOptions, Pool, Postgres};

pub(crate) async fn create_pg_pool(
    config: &Config,
) -> Result<Pool<Postgres>, sqlx::Error> {
    PgPoolOptions::new()
        .max_connections(5)
        .connect(&config.database.connection_string())
        .await
}
