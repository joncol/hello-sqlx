//!
#![warn(missing_debug_implementations, missing_docs, rust_2018_idioms)]

mod config;
mod database;
mod model;

use database::*;
use model::*;

#[tokio::main]
async fn main() -> Result<(), sqlx::Error> {
    let config = config::get_config().expect("failed to read configuration");

    let pool = create_pg_pool(&config).await?;

    truncate_cars_table(&pool).await?;
    create_car(&pool).await?;
    create_car(&pool).await?;

    let cs = cars(&pool).await?;
    println!("{:#?}", &cs);

    Ok(())
}
