Introduction
============

Experimentation with `sqlx` in Rust.

Requirements
------------
To handle migrations etc, `sqlx-cli` is utilized. Install it via:

```bash
cargo install sqlx-cli --no-default-features --features=postgres
```

Environment
-----------
The `sqlx` CLI tool requires the environment variable `DATABASE_URL` to be set
to the connection string. For example:

```bash
export DATABASE_URL=postgres://${DB_USER}:${DB_PASSWORD}@localhost:${DB_PORT}/${DB_NAME}
```

This is done in `.envrc`; to enable this, run `direnv allow`.

Creating the database
---------------------
The `init_db.sh` script takes care of running the Postgres database in a Docker
container.

Creating a migration
--------------------
A new migration can be created by running:

```bash
sqlx migrate add create_cars_table
```

Run the migration(s) with:

```bash
sqlx migrate run
```
