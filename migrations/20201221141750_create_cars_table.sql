-- Create cars table.
create table cars(
    id uuid not null,
    primary key(id),
    brand text not null,
    model text not null,
    year integer not null,
    created_at timestamp without time zone not null
);
